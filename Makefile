# DESTDIR is used to install into a different root directory
DESTDIR?=/
# Specify the kernel directory to use
KERNELDIR?=/lib/modules/$(shell uname -r)/build
# Need the absolute directory do the driver directory to build kernel modules
DRIVERDIR?=$(shell pwd)/driver

# Where kernel drivers are going to be installed
MODULEDIR?=/lib/modules/$(shell uname -r)/kernel/drivers/i2c/busses

DKMS_NAME?=i2c-nct6775
DKMS_VER?=0.1.0


# Build all target
all: driver

# Driver compilation
driver:
	@echo -e "\n::\033[32m Compiling i2c-nct6775 kernel module\033[0m"
	@echo "========================================"
	$(MAKE) -C $(KERNELDIR) M=$(DRIVERDIR) modules

driver_clean:
	@echo -e "\n::\033[32m Cleaning i2c-nct6775 kernel module\033[0m"
	@echo "========================================"
	$(MAKE) -C "$(KERNELDIR)" M="$(DRIVERDIR)" clean

# Install kernel modules and then update module dependencies
driver_install:
	@echo -e "\n::\033[34m Installing i2c-nct6775 kernel module\033[0m"
	@echo "====================================================="
	@cp -v $(DRIVERDIR)/*.ko $(DESTDIR)/$(MODULEDIR)
	@chown -v root:root $(DESTDIR)/$(MODULEDIR)/*.ko
	depmod

# Just use for packaging openrazer, not for installing manually
driver_install_packaging:
	@echo -e "\n::\033[34m Installing i2c-nct6775 kernel module\033[0m"
	@echo "====================================================="
	@cp -v $(DRIVERDIR)/*.ko $(DESTDIR)/$(MODULEDIR)

# Remove kernel modules
driver_uninstall:
	@echo -e "\n::\033[34m Uninstalling i2c-nct6775 kernel module\033[0m"
	@echo "====================================================="
	@rm -fv $(DESTDIR)/$(MODULEDIR)/i2c-nct6775.ko

# Clean target
clean: driver_clean

setup_dkms:
	@echo -e "\n::\033[34m Installing DKMS files\033[0m"
	@echo "====================================================="
	install -m 644 -v -D Makefile $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)/Makefile
	install -m 644 -v -D dkms/dkms.conf $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)/dkms.conf
	install -m 755 -v -d driver $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)/driver
	install -m 644 -v -D driver/Makefile $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)/driver/Makefile
	install -m 644 -v driver/*.c $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)/driver/

remove_dkms:
	@echo -e "\n::\033[34m Removing DKMS files\033[0m"
	@echo "====================================================="
	rm -rf $(DESTDIR)/usr/src/$(DKMS_NAME)-$(DKMS_VER)

# Install for Ubuntu
ubuntu_install: setup_dkms

install_i_know_what_i_am_doing: all driver_install

install: manual_install_msg ;

manual_install_msg:
	@echo "Please do not install the driver using this method. Use a distribution package as it tracks the files installed and can remove them afterwards. If you are 100% sure, you want to do this, find the correct target in the Makefile."
	@echo "Exiting."

uninstall: driver_uninstall


.PHONY: driver
